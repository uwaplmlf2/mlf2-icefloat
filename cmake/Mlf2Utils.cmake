include_guard()

#
# Generate a config.h header file from a template and a list
# of devices (sensors) used in this deployment.
#
function(utils_gen_config devfile tmpl cfgfile)
  file(STRINGS ${devfile} CONTENT)
  foreach(line ${CONTENT})
    string(REPLACE " " ";" NAME "${line}")
    list(LENGTH NAME N)
    list(GET NAME 0 dev)
    string(TOUPPER "${dev}" dev)
    if(N EQUAL 1)
      set(HAVE_${dev} "1")
    else()
      list(GET NAME 1 val)
      set(HAVE_${dev} "${val}")
    endif()
  endforeach()
  configure_file(${tmpl} ${cfgfile})
endfunction()

#
# Generate the mission-type header file from a template and the
# mission short name (slug)
#
function(utils_gen_mission_file mtype tmpl outfile)
  string(TOUPPER "${mtype}" MISSION)
  set(${MISSION} "1")
  configure_file(${tmpl} ${outfile})
endfunction()
