find_library(
  LIBTT8_LIBRARY
  NAMES libltt8.a
  HINTS /usr/local/mlf2/lib ${PROJECT_BINARY_DIR}/prebuilt/mlf2/lib/ ENV TT8LIBDIR
  NO_DEFAULT_PATH)

find_path(
  LIBTT8_INCLUDE_DIR
  NAMES adintf.h lpsleep.h
  HINTS /usr/local/tt8/include ${PROJECT_BINARY_DIR}/prebuilt/mlf2/include/ ENV TT8INCLUDE)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(libtt8 DEFAULT_MSG
  LIBTT8_LIBRARY
  LIBTT8_INCLUDE_DIR)

mark_as_advanced(
  LIBTT8_LIBRARY
  LIBTT8_INCLUDE_DIR)

if(LIBTT8_FOUND AND NOT TARGET libtt8::libtt8)
  add_library(libtt8::libtt8 STATIC IMPORTED)
  set_target_properties(
    libtt8::libtt8
    PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${LIBTT8_INCLUDE_DIR}"
    IMPORTED_LOCATION "${LIBTT8_LIBRARY}")
endif()
