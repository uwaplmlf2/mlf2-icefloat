find_library(
  LIBMLF2_LIBRARY
  NAMES libmlf2v2_1.a
  HINTS /usr/local/mlf2/lib ${PROJECT_BINARY_DIR}/prebuilt/mlf2/lib/ ENV MLF2LIBDIR
  NO_DEFAULT_PATH)

find_path(
  LIBMLF2_INCLUDE_DIR
  NAMES ad7714.h drogue.h
  HINTS /usr/local/mlf2/include ${PROJECT_BINARY_DIR}/prebuilt/mlf2/include/ ENV MLF2INCLUDE)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(libmlf2 DEFAULT_MSG
  LIBMLF2_LIBRARY
  LIBMLF2_INCLUDE_DIR)

mark_as_advanced(
  LIBMLF2_LIBRARY
  LIBMLF2_INCLUDE_DIR)

if(LIBMLF2_FOUND AND NOT TARGET libmlf2::libmlf2)
  add_library(libmlf2::libmlf2 STATIC IMPORTED)
  set_target_properties(
    libmlf2::libmlf2
    PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${LIBMLF2_INCLUDE_DIR}"
    IMPORTED_LOCATION "${LIBMLF2_LIBRARY}")
endif()
