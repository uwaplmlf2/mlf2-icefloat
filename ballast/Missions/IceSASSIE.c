/************************************************************************************************************/
/*                               Ice Float functions														*/
/************************************************************************************************************/

/* Process the new surfcheck result and check the surfacing rules */
/* Will set Ice.ok_to_surface and return 1 if the mode needs to be terminated ASAP */
short check_surfacing_rules(double day_time, int surfcheck_result)
{
	time_t now;
	struct tm  *tm;
	double yday;
	int ie; // ice epoch
	double ice_draft = 100.;
	short surface_flag = -1; // -1: uncertain, 0: open water, 1: ice
	
	if (!Ice.Run)
	{ // shouldn't happen! This probably means the result arrived late
		// log an exception and ignore
		log_event("Ice [%d] unexpected\n", surfcheck_result);
		return 0;
	}

	// Get real time
	time(&now);
	tm = gmtime(&now);
	yday = (double)tm->tm_yday + (double)tm->tm_hour / 24.0 + (double)tm->tm_min / 24.0 / 60.0 + (double)tm->tm_sec / 24.0 / 60.0 / 60.0;
	
#ifdef SIMULATION
	yday = day_time; // reset yday so that this works during simulated missions
#endif

	
	// find the ice "epoch" (0..N_ICE_EPOCH-1) corresponding to yday
	for (ie = 0; ie < N_ICE_EPOCH && yday > Ice.EpochYearDay[ie]; ie++){};
		
	// Process the new surfcheck result
	if (surfcheck_result>0) 
	{ // not an error
		surface_flag = surfcheck_result & 1;
		ice_draft = (double)(surfcheck_result >> 1) / Ice.IceReturnScale+Ice.Patm-Ice.PatmUsed;
	}
	Ice.Count++;
	
	if (ie == N_ICE_EPOCH)
	{
		Ice.okToSurface = 1;
		log_event("Ice [%d] d:%.2f f:%d ie:%d * Always surface *\n", surfcheck_result, ice_draft, surface_flag, ie);
		return 1;
	}

		/* Check the rules:	*/
	if (surface_flag==0)
	{
		// open water return is always a green flag
		Ice.GreenCount++;
			}
	if (surface_flag>0)
	{ // ice return = red flag (for now, may want to check thickness)
		// reset green flag counter
		Ice.GreenCount = 0;
			}
	if (surface_flag<0 && !Ice.IgnoreBad[ie])
	{ // this was a bad return in epoch when they were not ignored = red flag
		// reset green flag counter
		Ice.GreenCount = 0;
		
	}
	
	/* Final verdict: */
	Ice.okToSurface = (Ice.GreenCount >= Ice.NeedGreenCount[ie]);
	log_event("Ice [%d] d:%.2f f:%d ie:%d a:%d (%d/%d) %s\n", 
		surfcheck_result, ice_draft, surface_flag, ie, Ice.act_now, Ice.GreenCount, Ice.Count, (Ice.okToSurface) ? "* Ok to surface *" : "*  No  *");

	// now, return "1" if the current mode needs to be terminated:
	return Ice.act_now || Ice.okToSurface;

}

/* Reset ice okToSurface and GreenCount
   Also, disable Ice.Run (need to enable explicitly)
*/
void  ice_reset()
{
	Ice.okToSurface = 0;
	Ice.act_now = 0;
	Ice.Count = 0;
	Ice.GreenCount = 0;
	Ice.day_time_last = -1;
	Ice.Run = 0;
}