
/* Sampling subroutine: for SASSIE Ice Float (Apr 2022)
Set float sampling scheme for this mission
Call on every data loop
*/




int sampling(int nmode, double day_time, double day_sec, double mode_time)
{
    double next_mode_time; 
    int result;

    SET_STATE_VAR(0); // set normal state variation, then we might set the bits to indicate variations - CTD profile (1), Ice avoidance (2), surfcheck (4), photo (8)

    // CTD profiling control
    if (CTDprof.Run) { // CTD profiling is potentially active in this mode!
        if (!ctd_profile_active() && PressureG < CTDprof.onDepth) { //not yet running, but the onDepth has been reached - start up
            log_event("%5.3f P=%.1f CTD prof start\n", day_time, PressureG);
            ctd_start_profile();
        }
    }
    else if (ctd_profile_active() && PressureG > CTDprof.offDepth) { // CTD profiling should not be active, but it is still running and we've reached the offDepth  -- shut it down
        ctd_stop_profile();
        log_event("%5.3f P=%.1f CTD prof end\n", day_time, PressureG);
    }
    SET_STATE_VAR(ctd_profile_active()?1:0); // CTD profile mode variation


    // ice avoidance when requested. Would we have problems getting to the surface when we need to? Not really, but the mode will advance when IceAvoid.Margin depth is reached
    if (IceAvoid.Run && PressureG <= IceAvoid.Pmax) {
        float AltRange, AltPressure, H;
        get_last_altimeter(&AltRange, &AltPressure); // obtain the altimeter readings
        if (AltRange > 0 && AltPressure > 0) {
            // change the top of the current profile if there is a valid range 
            H = AltPressure - AltRange; // estimated pressure at ice bottom. Use altimeter pressure, since it is coincident with the range (and both may be old)
            Prof.Ptarget = H + IceAvoid.Margin;
            log_event("%5.3f Ice avoidance  P=%.1f Pa=%.1f Ra=%.1f Ptarget=%.1f\n", day_time, PressureG, AltPressure, AltRange, Prof.Ptarget);

        }
        TOGGLE_STATE_VAR(2); // ice-avoidance state variation 
    }

    // Ice logic still needs to be revisited
    Ice.SurfcheckRequested = 0;
	// deal with the altimeter
    // Notice that Ice.act_now needs to disable Ice.DelaySec. At the same time, we cannot always request surfcheck if Ice.act_now>0, since we only want to request it once (Ice.IntervalSec applies to subsequent requests)
	if (Ice.Run)
	{
        if ( (mode_time*86400.> Ice.DelaySec || Ice.act_now) && (day_time - Ice.day_time_last)*86400. > Ice.IntervalSec )
		{
            set_param_int("alt:tsample", (long)Ice.DurationSec); // Altimeter burst duration
            req_surfcheck();
            log_event("%5.3f P=%.1f SFC check\n", day_time, PressureG);
			Ice.day_time_last = day_time;
            Ice.SurfcheckRequested = 1;

             // Check if this is the last run before timeout (assume we are in DRIFT_P mode, which is BAD!).
            next_mode_time = (long)(mode_time * 86400.) + Ice.IntervalSec + Si[MODE_DRIFT_P];
            if (next_mode_time > Drift_P.timeout){
                log_event("Last SFC check before timeout (next @ %5.0f>%5.0f s)\n", next_mode_time, Drift_P.timeout);
                Ice.act_now = 1;  // Last run... Request "act_now"
            }
            Snapshot.Force |= Ice.act_now; // always take a photo on a last run (indicated by act_now)
			TOGGLE_STATE_VAR(4); // set state variation to indicate the surface check
		}
	}


    // photo-taking
    
    if (Snapshot.Run) {
        // check the time of day
        if ((Snapshot.TimeMax > Snapshot.TimeMin && day_sec >= Snapshot.TimeMin && day_sec <= Snapshot.TimeMax) ||
            (Snapshot.TimeMax <= Snapshot.TimeMin && (day_sec >= Snapshot.TimeMin || day_sec <= Snapshot.TimeMax))) {
            // check interval
            if (Snapshot.Force || (long)((day_time - Snapshot.day_time_last) * 86400.) >= Snapshot.Interval) {
                // check depth
                if ( PressureG >= Snapshot.Pmin && PressureG <= Snapshot.Pmax )  {
                    req_snapshot();
                    log_event("%5.3f P=%.1f Snapshot\n", day_time, PressureG);
                    Snapshot.Force = 0;
                    Snapshot.day_time_last = day_time; // maybe this needs to be done only on successful snapshots?
                    TOGGLE_STATE_VAR(8); // set state variation to indicate the cycle the snapshot was taken
                }
            }
        }
    }

return(0);
}
