/*****************  FloatECO (aka Garbage Patch, aka Camera Float)  ***************************/
/* Sampling subroutine: Called on every data loop to set float sampling scheme for this mission */
 
// June 2019

#ifdef SIMULATION
    int snapshot(void) {};
#endif


int sampling(int nmode, double day_time, double day_sec, double mode_time)
{
	static double ProfileStartTime = 0; // store the time a profile was started
	int result;
	
    
    if (SNAPSHOT.Run)
	{
        if (PressureG >= SNAPSHOT.Pmin &&
            PressureG <= SNAPSHOT.Pmax &&
            nmode >= SNAPSHOT.MODEmin)
        { 
            log_event("Snapshot window reached (P=%.1f, M=%d), activating camera\n", PressureG,nmode);
            result = snapshot();
            log_event("Snapshot done (%d)\n", result);
            SNAPSHOT.Run = 0; 
            SET_STATE_VAR(1); // set state variation to indicate the cycle the snapshot was taken
        }
    }
     else {
          SET_STATE_VAR(0); // set normal state variation 
     }
         
            
    
return(0);
}
