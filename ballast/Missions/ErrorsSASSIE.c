/*
** Handle the MODE_ERROR state for SASSIE mission.
*/
/*
#include <stdio.h>
#include <stdlib.h>
#include "ballast.h"
#ifndef SIMULATION
    #include "log.h"
    #include "ptable.h"
    #include "abort.h"
#else
    #include "ptable_mex.h"
#endif
*/

#ifdef SIMULATION
    #define log_event printf
    void abort_mission(int now){ log_event(" ============ Real mission would abort here! ============\n"); };
#endif

extern short int stage; //Stage is defined and used in mission code. (Perhaps error handling should be incorporated there as well?)
extern short newmode; //also defined and used in mission code

void swap_pressure_sensors(void)
{ // swap pressure sensors
    int pp;
    pp = get_param_as_int("primary_pr");
    log_event("Switching primary pressure sensor %d->%d\n", pp, (1 - pp));
    set_param_int("primary_pr", 1 - pp);
}

/*
 * Decide the appropriate transition from MODE_ERROR based on the
 * error type-code passed by the caller. The return value will be
 * the new mission mode.
 *
 * Error type-codes are defined in ballast.h.
 * 
 * A way to ignore an error is to call mode = next_mode(MODE_RESTORE,0)
 * If we need to switch to a special error-recovery stage (e.g., emergency ascent?), set   stage=x and call next_mode
 * 
 * We *must* switch out of MODE_ERROR!
 */
int handle_mode_error(error_t err)
{
    int         mode = MODE_ERROR;
    log_event("MODE_ERROR(%d) mitigation...\n", (int)err);
    switch(err) {
        /* these errors will be IGNORED! */
        case ERR_AUX:
            mode = next_mode(MODE_RESTORE, 0);
            break;

        /* these errors will cause "soft" mission restart 
        Ideally, we'd like to restore the default parameters, but to do that we'd need to process mission.xml and params.xml 
        (because there may be parameters superseding the hard-wired ones).
        So just a soft restart*/
        case ERR_INVALID_MODE:
        case ERR_INVALID_STAGE:
        case ERR_INVALID_ICALL:
            stage = STAGE_ICE_PROFILE;
            mode = MODE_PROFILE_UP; 
            log_event("Soft restart (STAGE=%d)\n",stage);
            break; 
        
        /* these errors will trigger emergency ascent (Stage 1), unless we already executing it: */
        case ERR_PRESSURE:
        case ERR_PRESSURE_RANGE:
            if (stage != STAGE_EMERGENCY_UP) {
                set_param_int("down_home", 0); // cancel homing (if any)
                stage = STAGE_EMERGENCY_UP;
                mode = next_mode(MODE_START, 0);
            }
            else {
                mode = next_mode(MODE_RESTORE, 0); // continue with STAGE_EMERGENCY_UP
            }
            break;
        
        
        /* these errors will trigger mission abort, release the weight, and restart in recover mode */
        case ERR_PISTON:
        case ERR_MOTOR_STALL:
        case ERR_MOTOR_LIMIT:
        case ERR_BATTERY12:
        case ERR_BATTERY15:
            mode = MODE_DONE;
            log_event("Aborting the mission\n");
            abort_mission(1);
            break;

        /* If the COMM is overdue, try to surface. Also reset the ICE EPOCH to disable surfchecks */
        case ERR_COMM_OVERDUE:
            if (Ice.EpochYearDay[N_ICE_EPOCH] > 0) {
                Ice.EpochYearDay[N_ICE_EPOCH] = 0;
                mode = MODE_COMM;
                log_event("Surfacing in COMM\n");
            }
            else {
                mode = next_mode(MODE_RESTORE, 0); // Epoch has been reset
            }
            break;

        /* default response is MODE_COMM: */
        case ERR_BAD_CTD:   // this error should not be ignored, because the mode is stuck while CTD is bad. Can disable with CTD.BadMax=-1
        case ERR_ZERO_RHO:  // same, disable with RHOMIN=-1
        case ERR_FILE_OPEN:
        case ERR_FILE_WRITE:
        case ERR_HUMIDITY:
        case ERR_PRESSURE_SENSOR:
        case ERR_UNKNOWN:
        default:
            set_param_int("comm:waiting", 1);
            set_param_int("comm:timeout",18000);  // Wait for a long time
            mode = MODE_COMM;
            log_event("Surfacing in COMM\n");
            break;
    }

    //log_event("MODE_ERROR(%d): new mode %d\n", (int)err, mode);
    return mode;
}

