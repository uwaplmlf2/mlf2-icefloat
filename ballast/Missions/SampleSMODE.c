/*****************SMODE ***************************/
/* Sampling subroutine: Called on every data loop to set float sampling scheme for this mission */
 
// JULY 2020

#ifdef SIMULATION
	/* GPS control dummy plugs */
	void start_surf_gps (void){
		return;
	}	

	void  stop_surf_gps(void){
		return ;
	}
#endif 


int sampling(int nmode, double day_time, double day_sec, double mode_time)
{	
    if (PressureG <= GPS.Pmax && !GPS.Running){ 
		// turn on GPS
		start_surf_gps();
		GPS.Running = 1;
		log_event("GPS ON (P=%.1f)\n",PressureG);

        }
     else if (PressureG > GPS.Pmax && GPS.Running) {
		stop_surf_gps();
		GPS.Running = 0; 
		log_event("GPS OFF (P=%.1f)\n",PressureG);
     }

	 SET_STATE_VAR(GPS.Running); // set state variation to indicate the GPS cycle
	 

return(0);
}
