/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* PUGET SOUND BALLASTING MISSION */
/* NEED TO SET PARAMETERS SET IN InitializePUGETSOUNDBALLAST */
/* modified July 22, 2008 for more control in first dive */

int next_mode(int nmode, double day_time)
{
    static int icall=0;
    int oldmode;
    oldmode=nmode;
    if (nmode==MODE_START){
	log_event("next_mode:PUGETSOUNDBALLAST Mission Start\n");
	nmode=MODE_PROFILE_DOWN;
	return(nmode);
    }

    ++icall;
    switch(icall){
        case 1: nmode=MODE_PROFILE_DOWN; 
	    save1=Settle.timeout;/* captive dive  - short */
	    Settle.timeout=Steps.time0;
	    break;
        case 2: nmode=MODE_SETTLE;break;
        case 3: nmode=MODE_DRIFT_SEEK;
		Drift.timeout_sec=Drift.time2;   /* very short test drift mode */
		Drift.closed_time=Drift.timeout_sec+100.; /* don't open drogue in captive dive */
		break;
	case 4: nmode=MODE_PROFILE_UP;break;
	case 5: nmode=MODE_COMM;break;      /* Option to terminate here or remove linefloat */
	case 6: nmode=MODE_PROFILE_DOWN; 
	    Settle.timeout=save1; /* restore long settle */
	    break;
        case 7: nmode=MODE_SETTLE;break;
        case 8: nmode=MODE_DRIFT_SEEK;
		Drift.timeout_sec=1000.;  /* longer test drift mode */
		Drift.closed_time=200.;           /* open part way through */
		break;
	case 9: nmode=MODE_PROFILE_UP;break;
	case 10: nmode=MODE_COMM;break;   /* Pick up float here */
	case 11: nmode=MODE_DONE;break;
        default: nmode=MODE_ERROR;break;  /* can't get here */
    }
    log_event("next_mode: %d ->%d\n",oldmode,nmode);
    return(nmode);
}

#include "SampleHURR08.c"
