/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* OWS PAPA - Jan 2011 Mission - Starts with PS ballast component */
/* NEED TO SET PARAMETERS SET IN InitializePAPA.c */
/* Dec 25, 2010  */

int next_mode(int nmode, double day_time)
{
    static int icall=0;
    int oldmode,i;
    double x;
    oldmode=nmode;
    if (nmode==MODE_START){
	log_event("next_mode:PAPA Mission Start\n");
	nmode=MODE_PROFILE_DOWN;
	Steps.cycle=0;
	return(nmode);
    }

    ++icall;
    switch(icall){
        case 1: nmode=MODE_PROFILE_DOWN;
		/* normal PSBallast check out */
			save1=Settle.timeout;/* captive dive  - short */
			Settle.timeout=Steps.time0;
			break;
        case 2: nmode=MODE_SETTLE;
		Ballast.Vset=0;  /* do not set ballast.v0 based on this settle */
		break;
        case 3: nmode=MODE_DRIFT_SEEK;
		Ballast.Vset=1;  /* restore value - now set ballast.V0 */
		save2=Drift.timeout_sec;
		save3=Drift.closed_time;
		Drift.timeout_sec=Drift.time2;   /* very short test drift mode */
		Drift.closed_time=Drift.timeout_sec+100.; /* don't open drogue in captive dive */
		break;
	case 4: nmode=MODE_PROFILE_UP;
		Drift.timeout_sec=save2;
		Drift.closed_time=save3;
		break;
	case 5: nmode=MODE_COMM;
			log_event("next_mode:  COMM after first test dive\n");
			break;      /* Option to terminate here or remove linefloat */
	case 6: nmode=MODE_PROFILE_DOWN;
		Settle.timeout=save1; /* restore long settle */
		break;
	case 7: nmode=MODE_SETTLE;
				log_event("next_mode:  INITIAL BALLASTING\n");
		break;
	case 8: nmode=MODE_DRIFT_SEEK;
		save2=Drift.timeout_sec;
		save3=Drift.closed_time;
		Drift.timeout_sec=1000.;  /* longer test drift mode */
		Drift.closed_time=200.;           /* open part way through */
		break;
	case 9: nmode=MODE_PROFILE_UP;
		Drift.timeout_sec=save2;
		Drift.closed_time=save3;
		break;
		/* END OF BALLAST */

		/* ---------------------------  Main cycle ----------  */
	case 10: nmode=MODE_COMM;
		break;
	case 11: nmode=MODE_PROFILE_DOWN;
		break;
	case 12: nmode=MODE_SETTLE;
	    log_event("next_mode:  DAILY SETTLE TO GET V0\n");
		break;
	case 13: /* insert to middle of mixed layer */
		Up.Pend=0.5*(Ballast.MLtop + Ballast.MLbottom);
		nmode=MODE_PROFILE_UP;
		break;
	case 14: nmode=MODE_DRIFT_ML;
		log_event("next_mode:  DAILY DRIFT IN ML\n");
		break;
	case 15: nmode=MODE_PROFILE_UP;
		Up.Pend=2;  /* profile to surface */
		icall=9;
		break;

	default: nmode=MODE_ERROR;
		log_event("ERROR: next_mode Cant get here %d\n",icall);
		break;  /* can't get here */
    }
    log_event("next_mode: %d ->%d\n",oldmode,nmode);
    return(nmode);
}

#include "SamplePAPA.c"
