/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
   does not need to be modified when values change */
/* Do not change this variable */
static const char *__id__ = "$Id$";
#include <sys/types.h>
#include <time.h>

# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN  900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 65 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 10  /* Stop ballastlog after this many days in simulation */


/* Declare misc. local scalar parameters - all satellite settable.  Actual initialization is in initialize_mission_parameters()*/
static double Mass0 ;  /* initial mass */
static double Creep;  /* kg/day */
static double deep_control;
static double shallow_control;  /*  m^2/dbar */
static double bottom_P;  /* Depth to push out piston */
static double error_P; /* Depth to declare emergency */
static double top_P;   /* Minimum allowed depth */
short int stage;   /* Master mode control variable */
static double telem_Step;   /* Commanded change in target */
static double Home_days;    /* Home about this often */
static double MaxQuietDays; /*maximum number of days without a comm mode - ERROR - safety feature */
static double sleepduration;// Total sleep time sec <<<< This should be a Sleep structure, perhaps

static unsigned long State;  // this is a combination of Stage, icall(phase), Mode, and Variation bytes. E.g., "Variation" can indicate the dritf "flavor" (ISO/SEEK/ML, so we could get rid of those modes), or whether SSAL is active, or stuff like that.
#define SET_STATE(s,p,m,v) State=(((long)s<<24)|((long)p<<16)|((long)m<<8)|((long)v))
#define SET_STATE_STAGE(s) State=State^((State^((long)s<<24))&((long)0xFF<<24))
#define SET_STATE_PHASE(p) State=State^((State^((long)p<<16))&((long)0xFF<<16))
#define SET_STATE_MODE(m)  State=State^((State^((long)m<<8))&((long)0xFF<<8))
#define SET_STATE_VAR(v) State=State^((State^((long)v))&0xFF)
#define TOGGLE_STATE_VAR(v) State=State^( ((long)v)&0xFF )


#ifdef SIMULATION
	// mission-specific dummy functions for the simulator
	// void start_surf_gps(void) {};
	// void stop_surf_gps(void) {};
	int CTD_PROFILE_FLAG = 0; // normally, Mike's code handles this

	void ctd_start_profile(void) { CTD_PROFILE_FLAG = 1; log_event("Starting CTD profile\n"); };
	int  ctd_stop_profile(void) { CTD_PROFILE_FLAG = 0;  log_event("Stopping CTD profile\n"); return 1; };
	int ctd_profile_active(void) { return CTD_PROFILE_FLAG; };


	void req_surfcheck(void) {};
	void req_snapshot(void) {};
	int get_surfcheck_result(void) { return -1; };
	void get_last_altimeter(float* range, float* pr) { *range = PressureG; *pr = PressureG; };
#endif


/**************************************************************************************************
        Declare global mission-specific parameters - Tier II
   Mission is so simple that these are pretty simple also
****************************************************************************************************/

/* Initialize parameter values and link them with the Ptable */

static struct psballast {
	struct profile Down[2];
	struct settle Settle[2];
	struct drift Drift[2];
	struct profile Up;
} PSBallast;

static struct mission { 
    struct profile Down; // first down
    short ncycles;                // number of "up" cycles
    struct profile CycleUp;     // UP for the cycle
    struct profile CycleDown;   // DOWN for the cycle
    struct profile Up;      // Up before COMM
    struct drift_p Drift_P;
    struct profile EmergencyUp;
} Mission;

/* Initialize mission-specific sampling control structures (for CTD, camera, etc.)
   They will be used in Sample*.c
*/
static struct  ctdprof {
	// CTD profiling control
		double onDepth;	// start the CTD profile once we get shallower than this. NB: we can disable CTD profile by setting onDepth to -100!
		double offDepth;	// Depth to switch to regular CTD on the way down (i.e., in the mode with SSAL.Run=0)
		// private parameters for internal record-keeping (not exposed via Ptable)
		short Run;		// whether to attempt to run CTD profile. Set by nextmode for the appropriate profiling modes. Whether the CTD profiling is actually running at each data cycle is determined by other parameters
	} CTDprof;
	
	// camera control parameters
	static struct {
		double Pmin;    // minimum pressure for taking photos
		double Pmax;    // maximum pressure for taking photos

		double TimeMin;    // earliers daysec to take photos
		double TimeMax;    // latest daysec to take photos. If TimeMin>TimeMax, photos will be taken during the UTC night

		double Interval;	// minimum interval between the photos [s]
		double day_time_last; // last time a photo was taken

		// private parameters for internal record-keeping (not exposed via Ptable)
		short Run;		// whether to attempt to take snapshots (normally set by timer or some other way)
		short Force;		// take the snapshot ignoring the interval
	} Snapshot;

/* structure to keep track of ice detection (aka SURFCHECK) logic*/
// number of ice-condition "ecpochs"
// each ends on a particular day and has its own rules
// last one has just one rule: attempt to surface no matter what

# define N_ICE_EPOCH (3)

	static struct ice
	{
		double IntervalSec; // burst interval
		double DurationSec; // burst duration
		double DelaySec;		// delay before the first burst (after start of a mode, check against mode_time)
		double PatmUsed;			// Patm used by the altimeter, must match surfcheck.c!
		double Patm;			// Best guess of the actual Patm
		double IceReturnScale;	// Scaling used to return ice draft from surfcheck  (must match surfcheck.c!)
		double EpochYearDay[N_ICE_EPOCH]; // ending dates of each epoch;

		short NeedGreenCount[N_ICE_EPOCH]; // min. number of "green flags" to surface? (0 = surface regardless of the result)
		short IgnoreBad[N_ICE_EPOCH]; // Ignore bad returns (otherwise, each one resets the GreenCount)
		short act_now; // Flag set by the timer, indicates that the decision will be made based on the next run
		short okToSurface; // Latest verdict (remember to reset when in doubt!)
		short SurfcheckRequested; // Indicates that surfcheck was requested during the sampling cycle (1: altimeter only, 2: with photo. *Only used in simulator*)

		// private options...
		short Run; // enable altimeter bursts (set in next_mode, hard-wired)
		double day_time_last; // save day_time of the previous burst
		int Count; // number of results since last reset (for information)
		int GreenCount; // number of consecutive "green" flags (reset at the start of the mode)
	} Ice;

static struct ice_avoid
{
	double Pmax;		// try to use ice avoidance above this pressure
	double Margin;		// try to stay this far from the ice [m]
 	
	// private options...
	short Run; // enable ice avoidance (set in next_mode, hard-wired)
} IceAvoid;



void initialize_mission_parameters(void){
    int k;
    struct profile Prof_Up, Prof_Down, Prof_Up_Fast, Prof_Down_Fast; // these are shorthand for the UP/DOWN profile defaults (they are only used to initialize the different Profile structures)

    /*******************************************/
    /* Sampling intervals
    ** Note that for the profiles, the "sampling interval" is now
    ** used to specify the maximum time allowed for ballast adjustment
    ** during each sample.
    */
    Si[MODE_PROFILE_UP] = 30;
    Si[MODE_PROFILE_DOWN] = 30;
  
	Si[MODE_SETTLE] = 30;
    Si[MODE_DRIFT_ISO] = 30;
    Si[MODE_DRIFT_ML] = 30;
    Si[MODE_DRIFT_SEEK] = 30;
    Si[MODE_SERVO_P] = 30;
    Si[MODE_DRIFT_P] = 30;
    Si[MODE_BOCHA] = 30;

	/************************************************************************************************
	Initialization of global (external) defaults
	*************************************************************************************************/
	set_param_int("down_home", 1);	// Always home at the end of DOWN
	set_param_int("aux:xfer_tmax", 600); // AUX file transfer timeout
    
    /************************************************************************************************
    Initialization of local static defaults
    ************************************************************************************************/
    deep_control = 25.e-6;
    shallow_control = 50.e-6;  /*  m^2/dbar */
    bottom_P = 150.;  /* Depth to push out piston */
    error_P = 220.; /* Depth to declare emergency */
    top_P = -100;   /* Minimum allowed depth */
    stage = 0;   /* Master mode control variable */
    telem_Step = 0.1;   /* Commanded change in target */
    Home_days = 1;    /* Home about this often */
    MaxQuietDays = 2; /*maximum number of days without a comm mode - ERROR - another safety feature */
    sleepduration = 8000;// Total sleep time sec

/* ------------------ EOS PARAMETERS --------------  */

	EOS.Mass0 = 50.;  /* initial mass  (kg) */
	EOS.V0 = 48600.0e-6; /* Estimated float volume at B=0, P=Pair, T=Tref, no air (m^3)*/
	EOS.Air = 15.5e-6;      /* Air volume at the surface (m^3)*/
	EOS.Compress = 3.1e-6;  /* float compressibility (dbar^-1)*/
	EOS.Thermal_exp = 0.722e-4;     /* float thermal expansion coeff */
	EOS.Tref = 8;      /* reference temperature */
	EOS.Creep = 0.e-6;  /* kg/day */
	EOS.CreepStart = 0; /* Days since the start of the mission to start the Creep */
	EOS.Pair = 10.;     /* Atmospheric pressure - nominally 10 (dbar)*/
	
	/* ------------------ DRAG PARAMETERS --------------  */
	DRAG.Cd = 1.0;	// Drag coefficient [non-dimensional]
	DRAG.Area = 0.55; // Cross-section area [m^2] * need to figure out what to do with the drogue
	DRAG.L = 1.0;	 // Length [m] (not necessarily the same as float length)

	/* ------------------ Error Control PARAMETERS --------------  */
    Error.Modes = -1;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    Error.Pmin = -1000;      /* Minimum Pressure  allowed */
    Error.Pmax = 1000;     /* Maximum Pressure  allowed */
    Error.timeout = 3600;    /* time outside of this range before error is declared*/

							 /* ------------------ CTD PARAMETERS --------------  */
	CTD.which = BOTTOMCTD;  /* How to get one CTD value from two CTD's - BOTTOMCTD ,TOPCTD, MEANCTD or MAXCTD */
	CTD.BadMax = -1;    /*How many bad CTDs before error   Long integer - Can be up to 2.1e9; -1 to disable*/
	CTD.Ptopmin = 2.1;      /* TopCTD not good above this pressure */
	CTD.Poffset = 0.0;      /* How much deeper is the float's center relative to the (primary) pressure sensor? [dbar] */
	CTD.TopSoffset = 0;    /* Correction to top CTD salinity */
	CTD.TopToffset = 0;   /*Correction to top CTD temperature*/
	CTD.TopPoffset = -0.832;   /*Top CTD pressure relative to the primary pressure sensor, typically<0 [dbar]*/
	CTD.BottomSoffset = 0;   /* Correction to Bottom CTD salinity */
	CTD.BottomToffset = 0;   /*Correction to Bottom CTD temperature*/
	CTD.BottomPoffset = 0.871;   /*Top CTD pressure relative to the primary pressure sensor, typically<0 [dbar]*/
	CTD.Separation = CTD.BottomPoffset - CTD.TopPoffset;   /* Distance between two CTDs*/

	/* ------------------ Mixed layer PARAMETERS --------------  */
	Mlb.go = 1;  /* 1 to compute MLB target, otherwise don't */
	Mlb.record = 0;   /* 1 to record data in arrays; else don't */
	Mlb.point = 0;       /* points at next open element in raw arrays, 0-> empty */
	Mlb.Nmin = 45;    /* minimum number of data to do computation */
	Mlb.dP = 2;     /* grid spacing m */
	Mlb.dSig = 0.02;  /* bin size for Sigma search */
	Mlb.Sigoff = 0.2; /* Offset of goal from ML density, final target = MLsigma + Sigoff */
    Mlb.SigStep = 0.1; /* Magnitude of secondary steps around goal  kg/m^3 */
					  //Mlb.Psave = 0; /* array of raw pressure */
					  //Mlb.Sigsave =0 ; /* array of raw potential density */
					  //Mlb.Pgrid = 0; /* array of gridded pressure */
					  //Mlb.Siggrid = 0; /* array of gridded potential density */

	/* ------------------ Bug control PARAMETERS --------------  */
	Bugs.start = -1;  /* sunset  / seconds of GMT day 0-86400 */
	Bugs.stop = -1;  /* sunrise */
	Bugs.start_weight = 0;  /* sunset weight / kg  */
	Bugs.stop_weight = 0;  /* sunrise weight /kg - linear interpolation between */
	Bugs.flap_interval = 10000;  /* time between flaps / seconds */
	Bugs.flap_duration = 130;  /* time between close and open */
	Bugs.weight = 0; /* weight - don't set  */

	/* ------------------ Timer PARAMETERS --------------  */
	Timer.enable = 0;       // ==1 if all timers are enabled (individual timers can be controlled by setting to -1  - This master is turned on/off in mission code to suppress timers)
	Timer.time[0] = -1;  // time (in seconds since midnight GMT) for the first timer event.
	Timer.time[1] = -1;       // ... for the second timer event
	Timer.time[2] = -1;       // ... for the third timer event
	Timer.time[3] = -1;       // ... for the fourth timer event
	Timer.command[0] = 0;    // timer command to issue. Usually, this is a stage number. For this mission, "1" will try to surface
	Timer.command[1] = 0;   // ... second timer
	Timer.command[2] = 0;   // ... third timer
	Timer.command[3] = 0;   // ... fourth timer
	Timer.nskip[0] = 0;   // How many timer triggers to skip? For multiday timing.
	Timer.nskip[1] = 0;   // timer2
	Timer.nskip[2] = 0;   // timer3
	Timer.nskip[3] = 0;   // timer4

	for (k = 0; k < NTIMERS; k++)
		Timer.countdown[k] = Timer.nskip[k]; // DON'T SET - internal countdown variable

   /************************************************************************************************
         Initialization of Tier 1 (default) mode parameters to serve as default templates
    *************************************************************************************************/
	Prof.Dir = 1;			// Profile direction: 1=UP, -1=DOWN
	Prof.timeout = 3600;		// Max duration 
	Prof.Ptarget = 3;		// Pressure to end the profile at
	Prof.ExtraTime = 0;		// time/sec past reaching Prof.Ptarget before end
	Prof.deltaB = 100e-6;	// Maximum bocha offset relative to the local equilibrium (proxy for max. profiling speed). Always positive
	Prof.W = 20e-3;			// Target profiling speed (absolute value) [m/s]
	Prof.Tau = 100;			// Time constant to be used in velocity control [s]
	Prof.WcontrolPmin = 0; // Min depth for W control [m]. Above this depth use "hard" (deltaB) control.
	Prof.Extrap = 1;			// Extrapolation parameter for predicting B0. =0 disables extrapolation, =1 extrapolates to the next data cycle, =0.5 to half-way
	Prof.Bmin = 0;		// min bocha setting
	Prof.Bmax = 660e-6;		// max bocha setting
	Prof.drogue = 0;		// drogue open (1) or closed (0) during the profile
	Prof.Speedbrake = 0;  // if 1, use drogue as speedbrake until float moves in the right direction

	// Shorthand templates for the UP/DOWN profile defaults (they are only used to initialize the different Profile structures)
	// First, profiles with speed control:
	Prof_Up = Prof;
	Prof_Up.Dir = 1;			// Profile direction: 1=UP, -1=DOWN
	Prof_Up.ExtraTime = 0;		// time/sec past reaching Prof.Ptarget before end
	
	Prof_Down = Prof;
	Prof_Down.Dir = -1;			// Profile direction: 1=UP, -1=DOWN
	Prof_Down.Ptarget = 100;	// Pressure to end the profile at
	
	// Next, full-speed profiles:
	Prof_Up_Fast = Prof_Up;
	Prof_Up_Fast.deltaB = 10 * Prof.Bmax; // large number = full speed
	Prof_Up_Fast.W = 0;

	Prof_Down_Fast = Prof_Down;
	Prof_Down_Fast.deltaB = 10 * Prof.Bmax; // large number = full speed
	Prof_Down_Fast.W = 0;
	
    Servo_P.Pmin=-1;   /* end if it goes above this depth */
    Servo_P.Pmax=100; /* end if it goes below this depth */
    Servo_P.Pgoal=6;  /* pull in Bocha if shallower than this*/
    Servo_P.Bspeed=0.007e-6; /* bocha increase speed when below Pgoal, m^3/s */
    Servo_P.BspeedEtime=1e4; /* efolding time to decrease Bspeed (sec) */
    Servo_P.BspeedShallow=0.4e-6;  /* bocha reduction speed when above Pgoal, m^3/s NB: this should be positive, ballast takes care of the sign*/
    Servo_P.timeout=2000;  /* timeout seconds */
    Servo_P.drogue=0;      /* drogue position: 0 closed, 1 open */

    Settle.secOday = -1;   /* end Settle when time passes this clock time [0 86400] (obsolete: replaced by timers) */
    Settle.timeout = 7200;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    Settle.seek_time = 0.8;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    Settle.decay_time = 0.4;      /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    Settle.nav = 20;                /* number of points to average to get settled volume */
	Settle.drogue_time = 0;// 300.; /* time after start of settle to keep drogue open */
    Settle.beta = 2; /* pseudo compressive gain (big for stable)*/
    Settle.tau = 400;             /* seek gain (big for stable)*/
    Settle.weight_error = 1;    /* Accept seek values if Dsig*V0 is     less than this (in kg)*/
    Settle.Vol_error = 0.5e-6;       /* 0.5e-6 Accept volume if stdev of V0 is less     than this ( if <0 no volume set) */
    Settle.Ptarget = 15;      /* Target Pressure - used only with Nfake or in Steps*/
    Settle.Nfake = 0;                   /* Fake stratification relative to Ptarget  No effect if 0 */
    Settle.nskip = 1;           /*      (if 0, skip settle mode always)  */
    Settle.SetTarget = 0; /* How to set target:  1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    Settle.Target = 1026.0;          /* Settle Target isopycnal */
    Settle.B0 = 50e-6;              /* bocha,  don't need to set */
    Settle.Bmin = 0.e-6; /* Minimum Bocha */
    Settle.Bmax = 660.e-6; /* Maximum Bocha */
	Settle.SetV0 = 0; /* if ==1, set EOS.V0 after the settle */

    Bocha.B = 150.e-6; /* Constant bocha to set*/
    Bocha.Btol = -1; /* Tolerance of Bocha. Set to -1 to disable exit-on-target-B */
    Bocha.timeout = 600.; /* Max duration sec*/
    Bocha.drogue = 0;    /* 0 or 1 - drogue open or closed  */
	Bocha.SetV0 = 0; /* if ==1, set EOS.V0 after the settle */

    Drift.SetTarget = 3; /*  How to set Target:     1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    Drift.VoffZero = 1;  /* 1 to set Voff=0 at drift start, else keep value */
    Drift.median = 1;  /* 1 use 5 point median filter, 0 don't */
    Drift.timetype = 2; /* How to end Drift mode 1: Use time since end of last drift mode or mission start 2: At the given timeOday (defaults to noon = 0.5) - Obsolete - Use Timers instead */
    Drift.time_end_sec = -1;   /* time/seconds to end Drift mode */
    Drift.timeout_sec = 50000.;  /* Additional timeout to end mode / seconds*/
    Drift.Voff = 0;       /* Ballast adjustment during Drift  */
    Drift.Voffmin = -200e-6;  /*prevent negative runaway on bottom */
    Drift.Target = 1024.0;         /* target isopycnal */
    Drift.iso_time = 7200;        /*seek time toward surface  sec  */
    Drift.seek_Pmin = -100;      /* depth range for drift seek mode*/
    Drift.seek_Pmax = -50;      /* continued */
    Drift.iso_Gamma = 1;   /*Pseudo-compressibility  m^3/unit, 1 = isopycnal*/
    Drift.time2 = 600;     /* second timeout parameter -- compatibility only, DO NOT USE  */
	Drift.closed_time = 100000;// 200;   /* drogue opens after this time   sec  */
	Drift.SetV0 = 0; /* if ==1, set EOS.V0 after the drift */


  	Drift_P.timeout = 60*60;		// Max duration 
	Drift_P.Ptarget = 10;		// Pressure to aim for
	Drift_P.Tau  = 300;			// Time constant to be used in velocity control [s]
	Drift_P.Pband = 5;		// P control band 
    Drift_P.W = 0.025;		// Vertical velocity range [m/s]; Velocity is set to +/-W outside Pband, reduced to zero within
	Drift_P.Extrap =1;		// Extrapolation parameter for predicting B0. =0 disables extrapolation, =1 extrapolates to the next data cycle, =0.5 to half-way
	Drift_P.Bmin = 0e-6;		// min bocha setting
	Drift_P.Bmax = 660e-6;		// max bocha setting


    Ballast.SetTarget = 1;  /* how to set Ballast.Target    1,2,3-no changes,    4- end of good Settle      5 - end of Drift, 6-end of Down, 7-end of Up,   8- Value at P=Ballast.Pgoal     9- Value from GetMLB (not implemented yet) */
    Ballast.MLsigmafilt = 0;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    Ballast.MLthreshold = -100;   /* Switch to ML ballasting if P<Pdev*[]     */
    Ballast.MLmin = 2;            /*        or if P<     */
    Ballast.MLtop = 2;          /* or between top and bottom */
    Ballast.MLbottom =10;
    Ballast.SEEKthreshold = -100;   /* Allow isoSEEK if P>Pdev*[]  & Pressure between Drift.seek_* limits   */
	Ballast.T0 = 26.8;      /* most recent temperature at ballasted point */
    Ballast.S0 = 37.45;      /* most recent Salinity  */
    Ballast.P0 = 0;      /* most recent Pressure*/
    Ballast.rho0 = 1024.628;    /* most recent water (=float) density (used to be important)*/
    Ballast.B0 = 160.e-6;      /* most recent bocha setting at ballasted point */
    Ballast.TH0 = 26.8;     /* most recent potential temperature */
    Ballast.Vdev = 0;  /* most recent stdev of float volume estimates */
    Ballast.Pgoal = 30;  /* if STP==2, set STP at this depth */
    Ballast.Target = 1022.4; /* target isopycnal e.g. 1022 */
  
  
/************************************************************************************************
Initialization of Tier 2 parameters.
These determine variations of the Mode parameters above for each particular stage
*************************************************************************************************/
/* ------------------ PS BALLAST STAGE PARAMETERS --------------  */
	PSBallast.Down[0] = Prof_Down_Fast;
	PSBallast.Down[0].Ptarget = 60;	// Depth of the first (captive) dive & drift

	PSBallast.Settle[0] = Settle;
	PSBallast.Settle[0].timeout = 300;	// length of the first (captive) settle - typically, very short 
	PSBallast.Settle[0].SetTarget = 1; // set settle target at end of dive -- this is where it settles 
	PSBallast.Settle[0].SetV0 = 0; // do not set EOS.V0 based on the first (captive) settle
	

	PSBallast.Drift[0] = Drift;
	PSBallast.Drift[0].timeout_sec = 100;	// length of the first (captive) drift - typically, very short 
	PSBallast.Drift[0].closed_time = PSBallast.Drift[0].timeout_sec + 100.; // don't open drogue in captive dive 

	PSBallast.Down[1] = Prof_Down_Fast;
	PSBallast.Down[1].Ptarget = 60;	// Depth of the second (longer) dive & drift

	PSBallast.Settle[1] = Settle;
	PSBallast.Settle[1].timeout = 10000;	// length of the second (longer) settle 
	PSBallast.Settle[1].seek_time = 8000;	// length of seeking time during settle
	PSBallast.Settle[1].SetTarget = 1; // set settle target at end of dive -- this is where it settles 
	PSBallast.Settle[1].SetV0 = 1; // DO set EOS.V0 after the second settle

	PSBallast.Drift[1] = Drift;
	PSBallast.Drift[1].timeout_sec = 1000;	// length of the second (longer) drift. 
	PSBallast.Drift[1].closed_time = PSBallast.Drift[1].timeout_sec / 5.; //open part way through
    
	PSBallast.Up = Prof_Up_Fast;

    Mission.Down = Prof_Down_Fast; // First (deep) DOWN
    Mission.Down.Ptarget = 100;
	Mission.Down.timeout = 3600;


    Mission.ncycles = 5; // number of "up" cycles
    
    Mission.CycleUp = Prof_Up; // 
	Mission.CycleUp.W = 10e-3; 
	Mission.CycleUp.deltaB = 100e-6; // limit extra buoyancy to avoid the "break-through" effect above the shallow picnocline
    Mission.CycleUp.Ptarget = 5;
    Mission.CycleUp.timeout = 5*3600; // allow enought time to ascend after home

	
    Mission.CycleDown = Prof_Down_Fast; // use the fast dive to improve the turnaround time
    Mission.CycleDown.Ptarget = 30;
	

    Mission.Up = Prof_Up; // regular UP, used before surfacing
	Mission.Up.Ptarget = 1;		// end the up at 1m 
	Mission.Up.ExtraTime = 5 * 30; // add some extra time to extend the CTD profile to the surface

    Mission.Drift_P = Drift_P;  // try to settle at ~5-10 m.
	Mission.Drift_P.timeout = 500*5;		// Max duration 
	Mission.Drift_P.Ptarget = 10;		// Pressure to aim for
	Mission.Drift_P.Tau  = 100;			// Time constant to be used in velocity control [s]
	Mission.Drift_P.Pband = 5;		// P control band 
    Mission.Drift_P.W = 0.025;		// Vertical velocity range [m/s]; Velocity is set to +/-W outside Pband, reduced to zero within
	Mission.Drift_P.Extrap =1;		// Extrapolation parameter for predicting B0. =0 disables extrapolation, =1 extrapolates to the next data cycle, =0.5 to half-way
	Mission.Drift_P.Bmin = 0e-6;		// min bocha setting
	Mission.Drift_P.Bmax = 660e-6;		// max bocha setting

	
    Mission.EmergencyUp = Prof_Up; // emergency UP, in response to over-pressure error
    Mission.EmergencyUp.Ptarget = 20; 
    Mission.EmergencyUp.deltaB = 50e-6;
    Mission.EmergencyUp.W = 0; //disable velocity control
    Mission.EmergencyUp.timeout = 4000; /* Max duration of up-leg   */

	// CTD profile parameters
	CTDprof.onDepth = 30;	// start the CTD profile once we get shallower than this. NB: we can disable CTD profile by setting onDepth to -100!
	CTDprof.offDepth = 5;	// Depth to switch to regular CTD on the way down (i.e., in the mode with SSAL.Run=0)
	CTDprof.Run = 0;		// whether to attempt to run CTD profile. Set by nextmode for the appropriate profiling modes. Whether the CTD profiling is actually running at each data cycle is determined by other parameters

	// Photo parameters
	Snapshot.Pmin = 1;    // minimum pressure for taking photos
	Snapshot.Pmax = 10;    // maximum pressure for taking photos
	Snapshot.TimeMin = 0;// (9 + 8) * 60 * 60;    // earliers daysec to take photos
	Snapshot.TimeMax = 86400;// (18 + 8 - 24) * 60 * 60;    // latest daysec to take photos. If TimeMin>TimeMax, photos will be taken during the UTC night
	Snapshot.Interval = 3600;// minimum interval between the photos [s]
	Snapshot.day_time_last = 0; // last time a photo was taken
	Snapshot.Run = 0;		// whether to attempt to take snapshots (normally set by timer or some other way)
	Snapshot.Force = 0;		// take the snapshot ignoring the interval


	Ice.IntervalSec = 600; // burst interval
	Ice.DurationSec = 500; // burst duration
	Ice.DelaySec = 0;		// delay before the first burst (after start of a mode, check against mode_time)
    Ice.PatmUsed = 9.90;			// Patm used by the altimeter, must match surfcheck.c!
	Ice.Patm = 10.0;			// Best guess of the actual Patm
	Ice.IceReturnScale = 10.0;  // Scaling used to return ice draft from surfcheck  (must match surfcheck.c!)
    Ice.SurfcheckRequested = 0; // Set internally to signal that surfcheck has been requested and we should expect surfcheck result
	Ice.Run = 0;
	
#ifdef SIMULATION
	Ice.EpochYearDay[0] = 0.5;// 243;//  1 Sep. 
	Ice.EpochYearDay[1] = 1;//250;//  8 Sep. 
	Ice.EpochYearDay[2] = 1.5;//257;// 15 Sep.
#else
    Ice.EpochYearDay[0] = 243;//  1 Sep. 
    Ice.EpochYearDay[1] = 250;//  8 Sep. 
    Ice.EpochYearDay[2] = 257;// 15 Sep.
#endif

	Ice.IgnoreBad[0] = 0; // Ignore bad returns? (otherwise, each one resets the GreenCount)
	Ice.IgnoreBad[1] = 0;
	Ice.IgnoreBad[2] = 0;

	Ice.NeedGreenCount[0] = 2; // min. number of "green flags" to surface?
	Ice.NeedGreenCount[1] = 1;
	Ice.NeedGreenCount[2] = 0;

	IceAvoid.Pmax = 15;		// use ice avoidance above this pressure
	IceAvoid.Margin = 1;	// try to stay this far from the ice [m]
	IceAvoid.Run = 0;
		

    /* Add mission-specific Ptble entries */
    /* These macros expand to add_param("Mission.Servo_P0.Pmin",	PTYPE_DOUBLE, &Mission.Servo_P0.Pmin), etc. */
    /* Use ADD_PTABLE_D for PTYPE_DOUBLE, ADD_PTABLE_L for PTYPE_LONG, and ADD_PTABLE_S for PTYPE_SHORT.*/

    /* PS Ballast */
	ADD_PTABLE_PROFILE(	PSBallast.Down[0]	);
	ADD_PTABLE_PROFILE(	PSBallast.Down[1]	);
	ADD_PTABLE_SETTLE(	PSBallast.Settle[0]	);
	ADD_PTABLE_SETTLE(	PSBallast.Settle[1]	);
	ADD_PTABLE_DRIFT(	PSBallast.Drift[0]	);
	ADD_PTABLE_DRIFT(	PSBallast.Drift[1]	);
	ADD_PTABLE_PROFILE(	PSBallast.Up		);


	ADD_PTABLE_PROFILE(	Mission.Down	);
    ADD_PTABLE_S(   Mission.ncycles     );


    ADD_PTABLE_PROFILE(	Mission.CycleUp	);
    ADD_PTABLE_PROFILE(	Mission.CycleDown	);
    ADD_PTABLE_PROFILE(	Mission.Up	);
    ADD_PTABLE_DRIFT_P(	Mission.Drift_P	);

    

    ADD_PTABLE_PROFILE(	Mission.EmergencyUp	);  

  
	// CTD profile parameters
	ADD_PTABLE_D(	CTDprof.onDepth	);
	ADD_PTABLE_D(	CTDprof.offDepth	);

	// Camera control parameters
	ADD_PTABLE_D(	Snapshot.Pmin		);
	ADD_PTABLE_D(	Snapshot.Pmax		);
	ADD_PTABLE_D(	Snapshot.TimeMin	);
	ADD_PTABLE_D(	Snapshot.TimeMax	);
	ADD_PTABLE_D(	Snapshot.Interval	);
	ADD_PTABLE_D(	Snapshot.day_time_last	);

	// Ice parameters
    ADD_PTABLE_D(   Ice.IntervalSec  );
    ADD_PTABLE_D(   Ice.DurationSec  );
    ADD_PTABLE_D(   Ice.DelaySec  );
    ADD_PTABLE_D(   Ice.PatmUsed  );
    ADD_PTABLE_D(   Ice.Patm  );

    ADD_PTABLE_D(   Ice.IceReturnScale  );
    ADD_PTABLE_S(   Ice.SurfcheckRequested  );

    ADD_PTABLE_D(   Ice.EpochYearDay[0]  );
    ADD_PTABLE_D(   Ice.EpochYearDay[1]  );
    ADD_PTABLE_D(   Ice.EpochYearDay[2]  );

    ADD_PTABLE_S(   Ice.IgnoreBad[0]  );
    ADD_PTABLE_S(   Ice.IgnoreBad[1]  );
    ADD_PTABLE_S(   Ice.IgnoreBad[2]  );

    ADD_PTABLE_S(   Ice.NeedGreenCount[0]  );
    ADD_PTABLE_S(   Ice.NeedGreenCount[1]  );
    ADD_PTABLE_S(   Ice.NeedGreenCount[2]  );

	// Ice avoidance parameters
	ADD_PTABLE_D(	IceAvoid.Pmax		);
	ADD_PTABLE_D(	IceAvoid.Margin		);
};
