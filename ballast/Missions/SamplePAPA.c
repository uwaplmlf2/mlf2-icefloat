/* Sampling subroutine: for PAPA
Set float sampling scheme for this mission
Call on every data loop
Turn off seabird O2 in drift if stage==1
*/
int sampling(int nmode, double day_time, double day_sec)
{
if (stage==1){
	DISABLE_SENSORS(MODE_DRIFT_ISO, SENS_O2);
	DISABLE_SENSORS(MODE_DRIFT_SEEK, SENS_O2);
	DISABLE_SENSORS( MODE_DRIFT_ML, SENS_O2);
}
else {
	ENABLE_SENSORS(MODE_DRIFT_ISO, SENS_O2);
	ENABLE_SENSORS(MODE_DRIFT_SEEK, SENS_O2);
	ENABLE_SENSORS( MODE_DRIFT_ML, SENS_O2);
}
return(0);
}
