cmake_minimum_required(VERSION 3.13)

if(NOT DEFINED CMAKE_TOOLCHAIN_FILE)
    message(FATAL_ERROR "Add -DCMAKE_TOOLCHAIN_FILE=cmake/mlf2gcc.cmake to command line")
endif()

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/")

# Determine float type from directory name
if(NOT DEFINED FLOAT_TYPE)
  string(REPLACE "/" ";" parts "${CMAKE_CURRENT_SOURCE_DIR}")
  list(GET parts -1 dirname)
  string(REGEX REPLACE "^mlf2-([a-zA-Z]+)" "\\1" FLOAT_TYPE "${dirname}")
endif()
message(STATUS "Float type: ${FLOAT_TYPE}")

project("mlf2${FLOAT_TYPE}" VERSION 1.0
  LANGUAGES C)

if(NOT DEFINED DEVFILE)
  set(DEVFILE "${PROJECT_SOURCE_DIR}/DEVICES")
endif()

if(NOT DEFINED MTYPE)
  message(FATAL_ERROR "Define mission type with -DMTYPE= command-line option")
endif()

set(CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}")
set(PKG_DIR "dist")

string(TOUPPER "${MTYPE}" MISSION_TYPE)

add_compile_definitions("NDEBUG=1" "__TT8__" "__PICODOS__")
add_compile_options(-mshort -fshort-enums -fno-builtin -Wall -Wstrict-prototypes)

include(Mlf2Utils)

set(gen_dir ${CMAKE_BINARY_DIR}/generated)
utils_gen_config(${DEVFILE}
  ${PROJECT_SOURCE_DIR}/cmake/config.h.in
  ${gen_dir}/config.h)
utils_gen_mission_file(${MISSION_TYPE}
  ${PROJECT_SOURCE_DIR}/cmake/mtype.h.in
  ${gen_dir}/mtype.h)

add_subdirectory(ballast)
add_subdirectory(mission)
add_subdirectory(recover)

set(ZIPFILE "${CMAKE_BINARY_DIR}/mlf2${FLOAT_TYPE}-${MTYPE}.zip")
file(GLOB PROG_FILES "${CMAKE_BINARY_DIR}/${PKG_DIR}/*")

add_custom_command(
  COMMAND ${CMAKE_COMMAND} -E tar "cf" "${ZIPFILE}" --format=zip -- ${PROG_FILES}
  OUTPUT ${ZIPFILE}
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/${PKG_DIR}"
  DEPENDS ${PROG_FILES}
  COMMENT "Archiving programs to ${ZIPFILE}")

add_custom_target(package DEPENDS "${ZIPFILE}")
