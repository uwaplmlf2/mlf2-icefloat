/*
** High-level interface to Aux Board.
**
** State Diagram description (see http://plantuml.sourceforge.net/state.html)
@startuml
[*] --> OFF
OFF --> BOOT : power-on
BOOT --> SHELL : booted
SHELL --> SHELL(PHOTO) : bgphoto
SHELL(PHOTO) --> SHELL : bgphoto done
SHELL --> SHELL(COMM) : call
SHELL(COMM) --> SHELL : call done
SHELL --> OFF : shutdown
@enduml
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "cpuclock.h"
#include "ptable.h"
#include "log.h"
#include "quote.h"
#include "ioports.h"
#include "ballast.h"
#include "fileq.h"
#include "iofuncs.h"
#include "aux_board.h"

#undef _VERBOSE_

static void report_error(const char *cmd);

/* Maximum length of a command-line from Shore */
#define MAX_LINE_LEN    128

static ab_state_t current_state = AB_OFF;
static long photo_pid = 0, comm_pid = 0, sc_pid = 0;
static char quotebuf[1024];
static time_t aux_board_tstart = 0;

/*
 * Set the SBC clock.
 */
int
aux_board_setup(void)
{
    cfsbc_set_clock();
    return cfsbc_dev_ready();
}



static void
report_error(const char *cmd)
{
    quote_string(cfsbc_shell_response(0L), quotebuf, sizeof(quotebuf));
    log_error("aux", "%s failed: %s\n", cmd, quotebuf);
}

#define PURGE_INPUT(d, t) while(sgetc_timed(d, MilliSecs(), (t)) != -1)

/*
 * s_getline - read a line from a serial device
 * @sdev: serial device number
 * @timeout: timeout in seconds
 * @linebuf: buffer for returned data
 * @n: maximum number of characters to read
 * @return 1 on success, 0 on timeout.
 *
 * Read the next linefeed terminated line from a serial device. The input
 * characters (minus the trailing linefeed) are written to the buffer and
 * terminated with a '\0'.
 */
static int
s_getline(int sdev, long timeout, char *linebuf, size_t n)
{
    ulong        start = MilliSecs(), ms_timeout;
    char        *p = linebuf;
    int         c, rval, text_started;

    rval = 1;
    text_started = 0;
    ms_timeout = (ulong)timeout*1000L;

    while(n--)
    {
        c = sgetc_timed(sdev, start, ms_timeout);
        if(c < 0)
        {
            rval = 0;
            break;
        }
        if(c == '\n')
            break;

        if(!text_started && !isspace(c))
            text_started = 1;

        if(text_started && c != '\r')
            *p++ = c;
        if((MilliSecs() - start) > ms_timeout)
        {
            rval = 0;
            break;
        }
    }

    *p = '\0';
    return rval;
}

/*
 * Start the Iridium mode along with a background process which will wait
 * for the modem to connect to the satellite network. Returns the process ID.
 */
long
aux_board_start_modem(void)
{
    long        pid = 0;

    if((current_state & AB_WAIT) != 0)
    {
        log_error("aux", "Modem startup already in progress\n");
        return pid;
    }

    power_on(COMM_POWER);
    current_state |= AB_MODEM;

    if(cfsbc_shell_command("commcheck\n", 3000L))
    {
        current_state |= AB_WAIT;
        cfsbc_int_response(0L, &pid);
    }
    else
        report_error("commcheck");
    // Make sure we are back at the shell prompt
    cfsbc_dev_ready();

    return pid;
}

void
aux_board_stop_modem(void)
{
    power_off(COMM_POWER);
    current_state &= ~AB_MODEM;
}


/*
 * Start the camera snapshot process. Returns the process ID.
 */
long
aux_board_snapshot(void)
{
    long                        pid = 0;
    struct camera_setup_t       cs;
    unsigned                    scales[1];

    if((current_state & AB_PHOTO) != 0)
    {
        log_error("aux", "Photo process [%ld] still running\n", photo_pid);
        return 0;
    }

    memset((void*)&cs, 0, sizeof(cs));

    scales[0] = (unsigned)get_param_as_int("photo:scale");
    if(scales[0] > 100)
        scales[0] = 100;
    cs.n_scales = 1;
    cs.exposure = get_param_as_double("photo:exposure");
    cs.scales = scales;

    if(cfsbc_photo(&cs, 5000L, 1) == 0)
    {
        report_error("bgphoto");
        return 0;
    }

    current_state |= AB_PHOTO;
    cfsbc_int_response(0L, &pid);
    photo_pid = pid;

    return pid;
}

/*
 * Start the surface check process and return the process ID.
 */
long
aux_board_surfcheck(unsigned duration)
{
    long        pid = 0;
    char        cmd[24];

    if((current_state & AB_SURFCHECK) != 0)
    {
        log_error("aux", "Surfcheck process [%ld] still running\n", sc_pid);
        return 0;
    }

    if(duration > 0)
        sprintf(cmd, "surfcheck %u\n", duration);
    else
        sprintf(cmd, "surfcheck\n");

    if(cfsbc_shell_command(cmd, 3000L))
    {
        cfsbc_int_response(0L, &pid);
        current_state |= AB_SURFCHECK;
        sc_pid = pid;
    }
    else
        report_error("surfcheck");

    return pid;
}

long
aux_board_call_server(const char *status_file, const char *imei)
{
    long        pid = 0;

    if(cfsbc_start_comms(status_file, imei) == 0)
    {
        report_error("call");
        return 0;
    }

    current_state |= AB_COMM;
    cfsbc_int_response(0L, &pid);
    comm_pid = pid;

    return pid;
}

long
aux_board_check_server(void)
{
    long        rval = 0;

    if(cfsbc_shell_command("svcheck\n", 7000L))
    {
        cfsbc_int_response(0L, &rval);
    }

    return rval;
}

/*
 * Remove the messages file.
 */
int
aux_board_clear_msgs(void)
{
    int  r;

    if((r = cfsbc_shell_command("clean messages\n", 3000L)) == 0)
        report_error("clean");
    return r;
}

/*
 * Check the status of a background process.  Return 1 if running, 0 if
 * done.
 */
long
aux_board_proc_running(long pid, int quiet)
{
    long         rval = -1;
    char        *resp, cmd[24];

    sprintf(cmd, "isrunning %ld\n", pid);

    if(cfsbc_shell_command(cmd, 3000L))
    {
        resp = cfsbc_shell_response(0L);
#ifdef _VERBOSE_
        quote_string(resp, quotebuf, sizeof(quotebuf));
        log_event("Shell response: %s\n", quotebuf);
#endif
        sscanf(resp, "%ld", &rval);
        if(!quiet)
            log_event("AUX: %s -> %ld\n", cmd, rval);
        if(rval == 0)
        {
            if(pid == photo_pid)
                current_state &= ~AB_PHOTO;
            else if(pid == comm_pid)
                current_state &= ~AB_COMM;
            else if(pid == sc_pid)
                current_state &= ~AB_SURFCHECK;
        }
    }
    else
        report_error("isrunning");

    return rval;
}

/*
 * Wait for a background process to finish or time limit to expire. Returns
 * the process exit code.
 */
long
aux_board_wait(long pid, long tlimit, int quiet)
{
    long        rval = EXIT_ERR, state;
    char        *resp, cmd[24];

    if(pid <= 0)
        return rval;

    while((state = aux_board_proc_running(pid, quiet)) != 0)
    {
        if(state == -1 || RtcToCtm() > tlimit)
            return rval;
        PET_WATCHDOG();
        DelayMilliSecs(500L);
    }

    sprintf(cmd, "wait %ld\n", pid);
    if(cfsbc_shell_command(cmd, 3000L))
    {
        resp = cfsbc_shell_response(0L);
#ifdef _VERBOSE_
        quote_string(resp, quotebuf, sizeof(quotebuf));
        log_event("Shell response: %s\n", quotebuf);
#endif
        sscanf(resp, "%ld", &rval);
        log_event("AUX: %s -> %ld\n", cmd, rval);
        if(pid == photo_pid)
        {
            current_state &= ~AB_PHOTO;
            photo_pid = 0;
        }
        else if(pid == comm_pid)
        {
            current_state &= ~AB_COMM;
            comm_pid = 0;
        }
        else if(pid == sc_pid)
        {
            current_state &= ~AB_SURFCHECK;
            sc_pid = 0;
        }
    }
    else
        report_error("wait");

    return rval;
}

/*
 * Terminate a background process and return its exit code.
 */
long
aux_board_kill(long pid)
{
    long        rval = EXIT_ERR;
    char        *resp, cmd[24];

    if(pid <= 0)
        return rval;

    sprintf(cmd, "kill %ld -TERM\n", pid);

    if(cfsbc_shell_command(cmd, 3000L))
    {
        resp = cfsbc_shell_response(0L);
#ifdef _VERBOSE_
        quote_string(resp, quotebuf, sizeof(quotebuf));
        log_event("Shell response: %s\n", quotebuf);
#endif
        sscanf(resp, "%ld", &rval);
        log_event("AUX: %s -> %ld\n", cmd, rval);
        if(pid == photo_pid)
        {
            current_state &= ~AB_PHOTO;
            photo_pid = 0;
        }
        else if(pid == comm_pid)
        {
            current_state &= ~AB_COMM;
            comm_pid = 0;
        }
        else if(pid == sc_pid)
        {
            current_state &= ~AB_SURFCHECK;
            sc_pid = 0;
        }
    }
    else
        report_error("kill");

    return rval;
}

/**
 * aux_board_xfer - transfer all queued files to the SBC. Files are removed
 * from the local CF card after a successful transfer.
 */
long
aux_board_xfer(long tlimit)
{
    int         count;
    long        dt, dtmax, t0;

    CPU_SET_SPEED(16000000L);
    DelayMilliSecs(1000L);

    dt = dtmax = 0;
    count = 0;
    while(fq_len() > 0 && (RtcToCtm() + dtmax) < tlimit)
    {
        PET_WATCHDOG();
        t0 = RtcToCtm();
        if(fq_send(cfsbc_archive_file, 0))
        {
            dt = RtcToCtm() - t0;
            if(dt > dtmax)
                dtmax = dt;
            count++;
        }
    }
    CPU_RESET_SPEED();

    return count;
}

action_t aux_board_proc_msgs(action_t (*callback)(void *obj, char *buf),
                             void *cbdata)
{
    int         mode, sdev = cfsbc_serialdev();
    action_t    ac = AC_NONE;
    static char linebuf[MAX_LINE_LEN];

    mode = serial_inmode(sdev, SERIAL_NONBLOCKING);
    sputz(sdev, "msgs?\n");
    PURGE_INPUT(sdev, 500L);

    while(s_getline(sdev, 2L, linebuf, sizeof(linebuf)-1))
    {
        if(linebuf[0] == '\0')
        {
            // Blank line signals end of input
            serial_inmode(sdev, mode);
            log_event("End of command list\n");
            return ac;
        }

        log_event("Command: \"%s\"\n", linebuf);
        ac |= callback(cbdata, linebuf);
    }

    serial_inmode(sdev, mode);
    return ac;
}

/*
 * Power-on the Aux Board. Returns 1 if successful, 0 on error.
 */
int
aux_board_power_on(void)
{
    if(cfsbc_init())
    {
        current_state = AB_BOOT;
        aux_board_tstart = RtcToCtm();
        return 1;
    }

    return 0;
}

/*
 * Return the uptime of the Aux Board in seconds.
 */
time_t
aux_board_uptime(void)
{
    return current_state == AB_OFF ? 0 : RtcToCtm() - aux_board_tstart;
}

/*
 * Power-off the Aux Board.
 */
void
aux_board_power_off(void)
{
    long        tlimit;

    aux_board_stop_modem();

    if((current_state & 0x0f) != AB_SHELL)
    {
        log_error("aux", "SBC not running\n");
        cfsbc_shutdown();
        return;
    }

    log_event("SBC shutting down\n");
    fflush(stdout);
    cfsbc_sleep(3600L);

    // Wait for clean shutdown
    tlimit = RtcToCtm() + 10L;
    do
    {
        if(cfsbc_done())
            break;
        PET_WATCHDOG();
        DelayMilliSecs(100L);
    } while(tlimit > RtcToCtm());
    log_event("Shutdown complete\n");
    cfsbc_shutdown();
    aux_board_tstart = 0;

    current_state = AB_OFF;
}

/*
 * Advance the Aux Board to the next state if possible. Returns the
 * current state.
 */
ab_state_t
aux_board_check_state(void)
{
    switch(current_state & 0x0f)
    {
        case AB_OFF:
            break;
        case AB_BOOT:
            if(cfsbc_dev_ready())
                current_state = AB_SHELL;
            break;
        case AB_SHELL:
            break;
    }

    return current_state;
}

/*
 * Return non-zero if the Aux Board is running a photo taking or server
 * comms operation.
 */
int
aux_board_is_busy(void)
{
    return (current_state & AB_PHOTO) |
      (current_state & AB_COMM) |
      (current_state & AB_SURFCHECK);
}

/*
 * Return non-zero if the Aux Board is powered on.
 */
int
aux_board_is_on(void)
{
    return current_state != AB_OFF;
}

/*
 * Cancel any running background tasks.
 */
void
aux_board_cancel(void)
{
    if(photo_pid > 0)
        aux_board_kill(photo_pid);
    if(comm_pid > 0)
        aux_board_kill(comm_pid);
    if(sc_pid > 0)
        aux_board_kill(sc_pid);
}

void
aux_board_show_output(const char *cmd)
{
    cfsbc_dev_ready();   // Get shell prompt
    if(cfsbc_shell_command(cmd, 10000L))
    {
        fputs(cfsbc_shell_response(0L), stdout);
    }
}
