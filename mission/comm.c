/*
** arch-tag: 550f1d8c-4802-47fb-830f-a3e5ad2c9c3e
**
** MLF2 comm-mode functions.
**
*/
#include "config.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tt8.h>
#include <picodcf8.h>
#include "util.h"
#include "nvram.h"
#include "ioports.h"
#include "lpsleep.h"
#include "ptable.h"
#include "log.h"
#include "gps.h"
#include "atod.h"
#include "rh.h"
#include "internalpr.h"
#include "netcdf.h"
#include "picalarm.h"
#include "message.h"
#include "sample.h"
#include "sensors.h"
#include "hash.h"
#include "fileq.h"
#include "msgq.h"
#include "compress.h"
#include "ballast.h"
#include "abort.h"
#include "comm.h"
#include "battery.h"
#include "serial.h"
#include "cpuclock.h"
#include "ad2cp.h"
#include "aux_board.h"


/* LED flasher activation */
#define FLASHER_ON()    iop_set(IO_C, 0x04)
#define FLASHER_OFF()   iop_clear(IO_C, 0x04)

#define ARGOS_ON()      iop_set(IO_C, 0x08)
#define ARGOS_OFF()     iop_clear(IO_C, 0x08)

#define MIN(a, b)       ((a) < (b) ? (a) : (b))

/*
** Tunable parameters. All timeouts are in seconds.
*/
static short comm_waiting = 0;
static long comm_reg_timeout = 180;
static long comm_wait_timeout = 7200;
static long comm_timeout = 1000;
static long comm_gps_timeout = 600;
static long comm_recovery_interval = 3600;
static long comm_wait_interval = 600;
static short comm_status_throttle = 0;
static short comm_gps_fixes = 30;
static long comm_max_disk_errors = 10;
static long safe_mode_duration = 0;
static long safe_mode_interval = 0;

INITFUNC(init_comm_params)
{
    add_param("comm:wait_timeout",      PTYPE_LONG, &comm_wait_timeout);
    add_param("comm:recovery_interval", PTYPE_LONG, &comm_recovery_interval);
    add_param("comm:gps_timeout",       PTYPE_LONG, &comm_gps_timeout);
    add_param("comm:status_throttle",   PTYPE_SHORT, &comm_status_throttle);
    add_param("comm:reg_timeout",       PTYPE_LONG, &comm_reg_timeout);
    add_param("comm:timeout",           PTYPE_LONG, &comm_timeout);
    add_param("comm:wait_interval",     PTYPE_LONG, &comm_wait_interval);
    add_param("comm:waiting",           PTYPE_SHORT, &comm_waiting);
    add_param("comm:gps_fixes",         PTYPE_SHORT, &comm_gps_fixes);
    add_param("comm:max_disk_errors",   PTYPE_LONG, &comm_max_disk_errors);
}


/*
 * write_status_file - write status message to a file
 * @type: message type, "status" or "recover".
 * @gdp: latest GPS position.
 *
 * Create an XML status message file to be sent to the remote system.
 * Returns 1 if successful, 0 if file creation fails.
 */
static int
write_status_file(const char *type, GPSdata *gdp)
{
    int         send_counts;
    float       lat, lon;
    time_t      t;
    long        size, dfree;
    FILE        *ofp, *mfp;
    struct tm   *tm;
    battery_state_t     batt[2];

    if((ofp = fopen("status.xml", "w")) == NULL)
    {
        log_error("comm", "Cannot open status file\n");
        return 0;
    }

    send_counts = strcmp(type, "status") ? 0 : 1;

    pdcfinfo("A:", &size, &dfree);

    t = time(0);
    tm = localtime(&t);

    /*
    ** Current date/time
    */
    fprintf(ofp, "<%s>", type);
    fprintf(ofp, "<date>%d-%02d-%02d %02d:%02d:%02d</date>",
                     tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
                     tm->tm_hour, tm->tm_min, tm->tm_sec);

    /*
    ** GPS location and satellite count.
    */
    lat = gdp->lat.deg;
    lat += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
        lat *= -1.0;

    lon = gdp->lon.deg;
    lon += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
        lon *= -1.0;

    fprintf(ofp, "<gps nsats='%d' fmt='deg'>%.6f/%.6f</gps>",
            gdp->satellites, lat, lon);
    if(gdp->status == 0)
        mq_add("Questionable GPS fix");

    /* Iridium signal quality */
    fprintf(ofp, "<irsq>0</irsq>");

    /* Free disk space */
    fprintf(ofp, "<df>%ld</df>", dfree);

    /*
    ** Relative humidity.
    */
    if(rh_init())
    {
        short   r;

        while(!rh_dev_ready())
            ;
        r = rh_read_data();
        rh_shutdown();

        fprintf(ofp, "<rh>%.1f</rh>", rh_mv_to_percent(r));
    }
    else
        mq_add("RH sensor failure");

    /*
    ** Internal pressure.
    */
    if(ipr_init())
    {
        short   r;

        while(!ipr_dev_ready())
            ;
        r = ipr_read_data();
        ipr_shutdown();

        fprintf(ofp, "<ipr>%.1f</ipr>", ipr_mv_to_psi(r));
    }
    else
        mq_add("IPR sensor failure");

    /*
    ** Battery voltages.
    */
    read_battery(BATTERY_12v, 10, &batt[0]);
    read_battery(BATTERY_15v, 10, &batt[1]);
    fprintf(ofp, "<v>%d/%d</v>", (int)(batt[0].v*1000),
                     (int)(batt[1].v*1000));

    /*
    ** Accumulated piston positioning error.
    */
    fprintf(ofp, "<ep>%ld</ep>",
                     get_param_as_int("piston_error"));

    /*
    ** Include "alert" messages.
    */
    if((mfp = fopen(MESSAGE_FILE, "r")) != NULL)
    {
        int     c;

        while((c = fgetc(mfp)) != EOF)
            fputc(c, ofp);
        fclose(mfp);
    }

    fprintf(ofp, "</%s>\n", type);
    fclose(ofp);

    return 1;
}

int
write_short_status(char *type, GPSdata *gdp, char *buf)
{
    char        *p;
    float       lat, lon;
    time_t      t;
    struct tm   *tm;
    battery_state_t     batt[2];

    p = buf;

    t = time(0);
    tm = localtime(&t);

    /*
    ** Current date/time
    */
    p += sprintf(p, "<%s>", type);
    p += sprintf(p, "<date>%d-%02d-%02d %02d:%02d:%02d</date>",
                 tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
                 tm->tm_hour, tm->tm_min, tm->tm_sec);
    /*
    ** GPS location and satellite count.
    */
    lat = gdp->lat.deg;
    lat += (((float)gdp->lat.min + ((float)gdp->lat.frac*1e-4))*0.0166667);
    if(gdp->lat.dir == 'S')
        lat *= -1.0;

    lon = gdp->lon.deg;
    lon += (((float)gdp->lon.min + ((float)gdp->lon.frac*1e-4))*0.0166667);
    if(gdp->lon.dir == 'W')
        lon *= -1.0;

    p += sprintf(p, "<gps nsats='%d' fmt='deg'>%.6f/%.6f</gps>",
            gdp->satellites, lat, lon);

    if(gdp->status == 0)
        p += sprintf(p, "<alert>Questionable GPS fix</alert>");

    /*
    ** Battery voltages.
    */
    read_battery(BATTERY_12v, 10, &batt[0]);
    read_battery(BATTERY_15v, 10, &batt[1]);
    p += sprintf(p, "<v>%d/%d</v>", (int)(batt[0].v*1000),
                 (int)(batt[1].v*1000));

    p += sprintf(p, "<alert>Disk write error</alert>");

    p += sprintf(p, "</%s>\n", type);
    *p = '\0';

    return (int)(p - buf);
}

/*
 * process_msg - process the command string from shore
 */
static action_t
process_msg(void *ignored, char *line)
{
    long                larg1, larg2;
    char                *args_p, *val_p;
    action_t            ac = AC_NONE;

    /* Split the line into a command and arguments */
    args_p = line;
    while(isalnum(*args_p) || ispunct(*args_p))
        args_p++;
    if(*args_p != '\0')
    {
        /* NULL terminated the command part */
        *args_p = '\0';
        args_p++;
        /* Skip whitespace */
        while(*args_p && isspace(*args_p))
            args_p++;
    }

    if(!strcmp(line, "flasher-on") ||
       (!strcmp(line, "flasher") && !strcmp(args_p, "on")))
    {
        FLASHER_ON();
    }
    else if(!strcmp(line, "flasher-off") ||
            (!strcmp(line, "flasher") && !strcmp(args_p, "off")))
    {
        FLASHER_OFF();
    }
    else if(!strcmp(line, "abort"))
    {
        ac |= AC_ABORT;
    }
    else if(!strcmp(line, "safe"))
    {
        if(args_p && sscanf(args_p, "%ld %ld", &larg1, &larg2) == 2)
        {
            if(larg1 > 0 && larg2 > 0)
            {
                ac |= AC_SAFE;
                safe_mode_duration = larg1;
                safe_mode_interval = larg2;
            }
        }

    }
    else if(!strcmp(line, "restart"))
    {
        ac |= AC_RESTART;
    }
    else if(!strcmp(line, "recover"))
    {
        ac |= AC_RECOVER;
    }
    else if(!strcmp(line, "wait"))
    {
        ac |= AC_WAIT;
        ac &= ~AC_CONTINUE;
    }
    else if(!strcmp(line, "continue"))
    {
        ac |= AC_CONTINUE;
        ac &= ~AC_WAIT;
    }
    else if(!strcmp(line, "get-errors"))
    {
        /*
         * Copy error messages to a compressed file and queue
         * for sending.
         */
        larg1 = 0;
        sscanf(args_p, "%ld", &larg1);
        log_event("Extracting log error messages\n");
        log_copy_errors("errors.txt", larg1);
        fq_add_uncompressed("errors.txt");
    }
    else if(!strcmp(line, "set"))
    {
        val_p = strchr(args_p, ' ');
        if(val_p)
        {
            *val_p = '\0';
            val_p++;
        }
        set_param_str(args_p, val_p);
    }
    else if(!strcmp(line, "release"))
    {
        larg1 = 0;
        sscanf(args_p, "%ld", &larg1);
        log_event("Releasing syntactic foam\n");
        iop_set(IO_B, 0x08);
        if(larg1 > 0 && larg1 <= 10)
            DelayMilliSecs(larg1*1000L);
        else
            DelayMilliSecs(2000L);
        iop_clear(IO_B, 0x08);
    }
    else if(!strcmp(line, "adcp:"))
    {
        FILE *ofp;
        /* Append to the ADCP command file */
        if((ofp = fopen(AD2CP_CMD_FILE, "a")) != NULL)
        {
            PET_WATCHDOG();
            fprintf(ofp, "%s\n", args_p);
            fclose(ofp);
        }
    }

    return ac;
}

static int
exec_action(action_t ac)
{
    if(ac & AC_WAIT)
    {
        comm_waiting = 1;
    }
    else if(ac & AC_CONTINUE)
    {
        comm_waiting = 0;
    }
    else if(ac & AC_SAFE)
    {
        enter_safe_mode(safe_mode_duration, safe_mode_interval);
    }
    else if(ac & AC_RESTART)
    {
        restart_sys();
    }
    else if(ac & AC_RECOVER)
    {
        enter_recovery_mode();
    }
    else if(ac & AC_ABORT)
    {
        abort_mission(1);
    }

    return (ac & AC_TIMEOUT) ? 0 : 1;
}

/*
 * call_server - communication procedure
 * @type: status message type ("status" or "recover")
 * @gps_on: if != 0, GPS is already powered-on.
 *
 */
static void
call_server(const char *type, int gps_on, const char *imei)
{
    long                t0, tlimit, mpid, pid;
    int                 allow_abort;
    action_t            ac = AC_NONE;
    GPSdata             gd;
    char                pbuf[32];

    ARGOS_ON();

    t0 = RtcToCtm();
    tlimit = t0 + comm_timeout;

    if(comm_waiting)
        mq_add("Waiting on surface");

    if(!strcmp(type, "recover"))
        allow_abort = 0;
    else
        allow_abort = 1;


    log_event("Checking GPS\n");
    memset(&gd, 0, sizeof(gd));
    if(!gps_on)
        gps_on = gps_init();

    if(!gps_on)
    {
        log_error("comm", "GPS initialization failed\n");
        mq_add("GPS failed");
    }
    else
    {
        sens_open_data_file(GPS_FILE, 0);
        sens_wait_for_gps(&gd, comm_gps_timeout, 1, comm_gps_fixes);
        sens_write_gps_fix(&gd);
        sens_close_file(GPS_FILE);
        gps_shutdown();
        gps_on = 0;
    }

    write_status_file(type, &gd);

    // COMM mode operations
    switch(aux_board_check_state())
    {
        case AB_OFF:
            aux_board_power_on();
        case AB_BOOT:
            log_event("Waiting for SBC to boot\n");
            while(aux_board_check_state() != AB_SHELL)
            {
                if(RtcToCtm() > tlimit)
                {
                    log_error("comm", "SBC failed to boot\n");
                    aux_board_power_off();
                    return;
                }
                DelayMilliSecs(500L);
            }
            log_event("SBC ready\n");
            aux_board_setup();
            break;
    }

    mpid = aux_board_start_modem();

    // Transfer all queued files
    if(fq_len() > 0)
        aux_board_xfer(tlimit);
    // Transfer the current parameter settings
    if(!aux_board_send_params("params.xml") && !aux_board_send_params("params.xml"))
        log_error("comm", "Parameter file upload failed\n");

    // Wait for the modem to connect to the Iridium network
    if(aux_board_wait(mpid, tlimit, 1) != EXIT_SUCCESS)
    {
        log_error("comm", "SBC modem not online\n");
        aux_board_kill(mpid);
        aux_board_stop_modem();
        goto cleanup;
    }

    // Wait for Internet routing to be established
    log_event("Checking server access\n");
    while(!aux_board_check_server())
    {
        PET_WATCHDOG();
        if(RtcToCtm() > tlimit)
        {
            aux_board_stop_modem();
            log_error("comm", "Cannot access server\n");
            goto cleanup;
        }
        DelayMilliSecs(500L);
    }

    // Start the session with the server
    log_event("Connecting to server\n");
    if((pid = aux_board_call_server("status.xml", imei)) == 0)
    {
        aux_board_stop_modem();
        log_error("comm", "Connection failed\n");
        goto cleanup;
    }
    aux_board_wait(pid, tlimit, 1);
    aux_board_stop_modem();
    log_event("Call complete\n");

    // Process commands from the server
    ac = aux_board_proc_msgs(process_msg, 0);
    aux_board_clear_msgs();

    if(!allow_abort)
        ac &= ~AC_ABORT;
    exec_action(ac);

cleanup:
    aux_board_power_off();

    if(fileexists(MESSAGE_FILE))
        unlink(MESSAGE_FILE);

    if(strcmp(type, "recover"))
    {
        ARGOS_OFF();
    }

}

/**
 * comm_mode - start a communication session with the server and optionally
 * wait on the surface.
 */
void
comm_mode(int gps_on, const char *imei)
{
    long        last_time = 0L, dt, t0;

    if(fileexists("telem.nc"))
        fq_add_uncompressed("telem.nc");

    if(fileexists("dataql.sx"))
        fq_add_uncompressed("dataql.sx");

    t0 = RtcToCtm();
    do
    {
        dt = comm_wait_interval - (RtcToCtm() - last_time);
        /*
         * Wait in low-power mode for the next communication interval. While
         * waiting, wake every 30 seconds and print a prompt to the console to
         * allow the user to interrupt the wait and perform an orderly restart
         * of the system.
         */
        while(dt > 0)
        {
            PET_WATCHDOG();
            fputs("Waiting for next COMM interval\n", stdout);
            if(isleep(MIN(dt, 30L)) == 1)
            {
                fputs("Control program will now restart\n", stdout);
                abort_prog();
            }
            dt = comm_wait_interval - (RtcToCtm() - last_time);
        }

        last_time = RtcToCtm();
        CPU_SET_SPEED(16000000L);
        call_server("status", gps_on, imei);
        CPU_RESET_SPEED();
        gps_on = 0;
        // Calling the server can result in comm_waiting and comm_wait_timeout
        // being modified.
    } while(comm_waiting == 1 && RtcToCtm() < (t0 + comm_wait_timeout));
    comm_waiting = 0;
}

void
comm_wait(int gps_on, const char *imei)
{
    comm_waiting = 1;
    comm_mode(gps_on, imei);
}

void
comm_recovery_mode(void)
{
    long        last_time = 0L, dt;

    comm_waiting = 0;

    while(1)
    {
        PET_WATCHDOG();
        /*
         * Wait in low-power mode for the next communication interval. While
         * waiting, wake every 60 seconds and print a prompt to the console to
         * allow the user to interrupt the wait and perform an orderly restart
         * of the system.
         */
        dt = comm_recovery_interval - (RtcToCtm() - last_time);

        while(dt > 0)
        {
            PET_WATCHDOG();
            fputs("Waiting to send RECOVERY message\n", stdout);
            if(isleep(MIN(dt, 60L)) == 1)
            {
                fputs("Control program will now restart\n", stdout);
                abort_prog();
            }
            dt = comm_recovery_interval - (RtcToCtm() - last_time);
        }

        last_time = RtcToCtm();
        CPU_SET_SPEED(16000000L);
        call_server("recover", 0, NULL);
        CPU_RESET_SPEED();

    }

}
