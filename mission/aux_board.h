#ifndef AUX_BOARD_H
#define AUX_BOARD_H

#include "cfsbc.h"
#include "power.h"
#include "comm.h"

#define EXIT_ERR        -127
#define EXIT_SUCCESS    0

typedef enum {
    AB_OFF=0,
    AB_BOOT=1,
    AB_SHELL=2,
    AB_PHOTO=0x10,
    AB_COMM=0x20,
    AB_WAIT=0x40,
    AB_MODEM=0x80,
    AB_SURFCHECK=0x100,
} ab_state_t;

#define aux_board_send_params(name)     cfsbc_send_params((name))
#define aux_board_show_log()            aux_board_show_output("log\n")

long aux_board_surfcheck(unsigned duration);
int aux_board_power_on(void);
void aux_board_power_off(void);
int aux_board_setup(void);
long aux_board_snapshot(void);
long aux_board_proc_running(long pid, int quiet);
long aux_board_wait(long pid, long tlimit, int quiet);
long aux_board_kill(long pid);
time_t aux_board_uptime(void);
ab_state_t aux_board_check_state(void);
long aux_board_xfer(long tlimit);
void aux_board_cancel(void);
long aux_board_start_modem(void);
void aux_board_stop_modem(void);
long aux_board_call_server(const char *status_file, const char *imei);
action_t aux_board_proc_msgs(action_t (*callback)(void *obj, char *linebuf),
                             void *cbdata);
int aux_board_clear_msgs(void);
void aux_board_show_output(const char *cmd);
long aux_board_check_server(void);
int aux_board_is_busy(void);
int aux_board_is_on(void);
#endif /* AUX_BOARD_H */
