/*
** $Id: comm.h,v 30ef1374b225 2007/04/15 03:14:25 mikek $
*/
#ifndef _COMM_H_
#define _COMM_H_

#ifndef WATCHDOG_TIMEOUT
#define WATCHDOG_TIMEOUT        900L
#endif

#ifndef __GNUC__
#define __attribute__(x)
#endif

typedef enum {
    AC_NONE=0,
    AC_WAIT=1,
    AC_CONTINUE=2,
    AC_SAFE=4,
    AC_RESTART=8,
    AC_RECOVER=16,
    AC_ABORT=32,
    AC_TIMEOUT=64
} action_t;

#define TELEM_LIST      "tlist.txt"

#define do_abort        abort_mission

void comm_mode(int gps_on, const char *imei);
void comm_wait(int gps_on, const char *imei);
void comm_recovery_mode(void) __attribute__ ((noreturn));

#endif
