#ifndef _ADCPMGR_H_
#define _ADCPMGR_H_

int adcp_pre_sample(int is_running);
int adcp_pre_comm(int is_running, int shutdown);
#endif
