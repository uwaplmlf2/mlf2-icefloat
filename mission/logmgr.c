/*
 * Manage the MLF2 log files. A maximum of 100 log files can be present
 * on the PicoDOS file system. The files are named syslogNN.txt where NN
 * ranges from 00 to 99.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <tt8lib.h>
#include "log.h"
#include "util.h"
#include "fileq.h"
#include "logmgr.h"

// Starting log file index
static int log_start = -1;
// Current log file index
static int log_index = 0;
// Time that the current log file was opened
static unsigned long log_time = 0;

/**
 * Initialize log file management. Search the file system for the first
 * unused log index number and use that as a starting point. Return the
 * index number of the next log file.
 */
int logmgr_init(void)
{
    int         i;
    char        filename[16];

    for(i = 0; i < MAX_LOG_FILES; i++)
    {
        sprintf(filename, "syslog%02d.txt", i);
        if(!fileexists(filename))
        {
            log_start = i;
            break;
        }
    }

    // All indicies are used, start from zero
    if(log_start == -1)
        log_start = 0;

    log_index = log_start;

    return log_start;
}

/**
 * Open the next log file.
 */
int logmgr_open_next(void)
{
    char        filename[16];

    if(log_start == -1)
        logmgr_init();

    closelog();
    sprintf(filename, "syslog%02d.txt", log_index);
    openlog(filename);
    log_time = RtcToCtm();

    return log_index;
}

/**
 * Return the age of the current log file in seconds.
 */
unsigned long logmgr_current_age(void)
{
    return RtcToCtm() - log_time;
}

/**
 * Close the current log file. Add the closed file to the outgoing
 * file queue unless noqueue is non-zero. Returns the index of the
 * next log file.
 */
int logmgr_close_current(int noqueue)
{
    char        filename[16];

    if(log_start == -1)
        return logmgr_init();
    closelog();
    sprintf(filename, "syslog%02d.txt", log_index);
    fq_add_uncompressed(filename);
    log_index = (log_index + 1) % MAX_LOG_FILES;

    return log_index;
}
