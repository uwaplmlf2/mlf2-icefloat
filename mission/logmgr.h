#ifndef _LOGMGR_H_
#define _LOGMGR_H_

#define MAX_LOG_FILES           100

int logmgr_init(void);
int logmgr_open_next(void);
int logmgr_close_current(int noqueue);
unsigned long logmgr_current_age(void);

#endif
