/**
 * Encapsulate Nortek ADCP control.
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#ifdef __GNUC__
#include <unistd.h>
#endif
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/fcntl.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include "log.h"
#include "ptable.h"
#include "cpuclock.h"
#include "fileq.h"
#include "ad2cp.h"

#ifdef __GNUC__
#define rename(a, b)    _rename(a, b)
int _rename(const char* old, const char *new);
#endif


static short    adcp_enable = 1;
static short    max_adcp_files = 10;
static long     adcp_maxsize = 50*1024L;


INITFUNC(init_adcp_params)
{
    add_param("adcp_enable",            PTYPE_SHORT, &adcp_enable);
    add_param("adcp_files",             PTYPE_SHORT, &max_adcp_files);
    add_param("adcp_maxsize",           PTYPE_LONG, &adcp_maxsize);
}

static int
start_adcp(int which, int keep)
{
    int     verbose, status;
    char    unitlog[16];

    sprintf(unitlog, "adcplog%1d.jsn", which);
    verbose = 0;

    CPU_SET_SPEED(16000000L);
    ad2cp_cmd(which, "ERASETM,9999\r", NULL, 0);
    status = ad2cp_deploy(which, keep, verbose);
    rename(AD2CP_LOG_FILE, unitlog);
    CPU_RESET_SPEED();

    return status;
}

static void
stop_adcp(int which, const char *filename)
{
    FILE    *fp;
    int     status;

    CPU_SET_SPEED(16000000L);
    if((fp = fopen(filename, "wb")) != NULL)
    {
        status = ad2cp_halt(which, fp, adcp_maxsize, 4096L);
        fclose(fp);
        if(adcp_maxsize > 0)
        {
            if(status > 0)
                fq_add_uncompressed(filename);
        }
        else
            unlink(filename);
    }
    else
        log_error("adcp", "Cannot store telemetry data to \"%s\"\n", filename);
    CPU_RESET_SPEED();

}

static void
next_adcp_file(int which, char *filename, size_t n)
{
    const char *template = "adcp%1d%03d.ntk";
    static unsigned _adcp_file_counter[] = {0, 0};

    if(max_adcp_files > 1000)
        max_adcp_files = 1000;
    snprintf(filename, n, template, which, _adcp_file_counter[which] % max_adcp_files);
    _adcp_file_counter[which]++;
}


int
adcp_pre_sample(int is_running)
{
    if(adcp_enable & 0x01)
    {
        if(ad2cp_init(0))
        {
            log_event("AD2CP Unit 0 initialized\n");
            is_running |= start_adcp(0, 1);
        }
        else
        {
            log_error("ad2cp", "Unit 0 disabled\n");
            ad2cp_shutdown(0);
            adcp_enable &= ~0x01;
        }

        if(is_running & 0x01)
            log_event("AD2CP Unit 0 running\n");
        else
        {
            log_error("adcp", "AD2CP Unit 0 disabled\n");
            ad2cp_shutdown(0);
        }
    }

    if(adcp_enable & 0x02)
    {
        if(ad2cp_init(1))
        {
            log_event("AD2CP Unit 1 initialized\n");
            is_running |= (start_adcp(1, 1) << 1);
        }
        else
        {
            log_error("ad2cp", "Unit 1 disabled\n");
            ad2cp_shutdown(1);
            adcp_enable &= ~0x02;
        }

        if(is_running & 0x02)
            log_event("AD2CP Unit 1 running\n");
        else
        {
            log_error("adcp", "AD2CP Unit 1 disabled\n");
            ad2cp_shutdown(1);
        }
    }

    return is_running;
}

int
adcp_pre_comm(int is_running, int shutdown)
{
    char        filename[16];

    if((adcp_enable & is_running) & 0x01)
    {
        next_adcp_file(0, filename, sizeof(filename)-1);
        stop_adcp(0, filename);
    }
    if((adcp_enable & is_running) & 0x02)
    {
        next_adcp_file(1, filename, sizeof(filename)-1);
        stop_adcp(1, filename);
    }
    is_running = 0;

    if(shutdown != 0)
    {
        ad2cp_shutdown(0);
        ad2cp_shutdown(1);
    }

    return is_running;
}
