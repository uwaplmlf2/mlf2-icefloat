/*
** Test COMM-mode
**
*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include "lpsleep.h"
#include "ioports.h"
#include "util.h"
#include "ptable.h"
#include "comm.h"
#include "fileq.h"
#include "log.h"
#include "version.h"

// Size of each entry returned by the "dir" PicoDOS command
#define DIR_ENTRY       24
#define DIRBUF_SIZE     (DIR_ENTRY*100L)
static char _dirbuf[DIRBUF_SIZE];

static void
queue_files(char *dbuf)
{
    char *p0, *p1;

    p0 = dbuf;
    while(*p0)
    {
        p1 = strchr(p0, ' ');
        if(!p1)
            break;
        *p1++ = '\0';
        log_event("Queueing %s\n", p0);
        fq_add_uncompressed(p0);
        p0 += DIR_ENTRY;
    }
}

static void
create_file_queue(void)
{
    char        *dbuf = &_dirbuf[0];
    char        cmd[32];

    sprintf(cmd, "dir *.nc /m %lx", (long)dbuf);
    execstr(cmd);
    queue_files(dbuf);
    sprintf(cmd, "dir *.sx /m %lx", (long)dbuf);
    execstr(cmd);
    queue_files(dbuf);
    sprintf(cmd, "dir *.txt /m %lx", (long)dbuf);
    execstr(cmd);
    queue_files(dbuf);
}


INITFUNC(sysinit)
{
    /*
    ** Hardware initialization
    */
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
        printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
        exit(1);

    SimSetFSys(16000000L);
    SerSetBaud(9600L, 0L);
}

static void
mlf2_qspi_init(void)
{
    register unsigned   cs_mask, cntl_mask;

    cs_mask   = M_PCS0 | M_PCS1 | M_PCS2 | M_PCS3;
    cntl_mask = M_SCK | M_MOSI | M_MISO;

    /*
    ** Configure the QSPI control and chip select lines as
    ** general purpose outputs. Force all lines except CS3
    ** low. CS3 is the chip-select for the MAX186 and is
    ** active low.
    */
    *QPAR = 0;
    *QPDR = M_PCS3;
    *QDDR = (cs_mask | cntl_mask);
}

int
main(int ac, char *av[])
{
    mlf2_qspi_init();

    init_sleep_hooks();
    init_param_table();
    init_ioports();

    printf("\n*** MLF2 Comm Test Program ***\n");
    openlog("commlog.txt");
    log_event("Software revision: %s\n", SHORT_REV);

    unlink("params.xml");

    if(yes_or_no("Transfer all data files", 0) == 1)
        create_file_queue();
    comm_mode(0, "0123456789");

    return 0;
}
